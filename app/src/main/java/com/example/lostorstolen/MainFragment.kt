package com.example.lostorstolen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.lostorstolen.carousel.CardCarousel
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(R.layout.fragment_main) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        card_carousel.adapter = CardCarousel(requireContext())
    }
}


