package com.example.lostorstolen.carousel

import androidx.annotation.DrawableRes
import com.example.lostorstolen.R

enum class CreditCards(@DrawableRes val resourceId: Int) {
    CREDIT(R.drawable.bg_credit_card),
    GOLD(R.drawable.bg_gold_card)
}