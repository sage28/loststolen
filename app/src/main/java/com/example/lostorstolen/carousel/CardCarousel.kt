package com.example.lostorstolen.carousel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.lostorstolen.R
import kotlinx.android.synthetic.main.item_carousel.view.*

class CardCarousel(private val context: Context) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any =
        LayoutInflater.from(context).inflate(R.layout.item_carousel, container, false)
            .apply {
                CreditCards.values()[position].resourceId
                    .let { imageResource ->
                        imageView.setImageDrawable(context.getDrawable(imageResource))
                    }
            }.also {
                container.addView(it)
            }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount() = CreditCards.values().size

}